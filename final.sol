pragma solidity >=0.4.22 <0.6.0;

contract RealEstate {
    enum Status { Closed, Selling, Gifted, Rented, WaitAccept }
    event changedStatus(uint16 id);

    address admin;

    struct Property {
        address owner;
        bytes32 name;
        uint16  area;
        uint16  u_area;
        uint    idOfDeal;
    }

    struct Deal {
        uint    price;
        uint    end_time;
        Status  status;
        address new_owner;
        bool    new_accept;
        uint    idOfProperty;
    }

    Property[] public properties;
    Deal[] public deals;
    mapping(address => uint) pendingReturns;

    constructor() public {
        admin = msg.sender;
        deals.push(
          Deal({
            price: 0,
            end_time: 0,
            status: Status.Closed,
            new_owner: address(0),
            new_accept: false,
            idOfProperty: 0
          }));
    }

    function getArrayLength() public view returns (uint) {
        return properties.length;
    }

    function getArrayOfIndexWhichForSale() public view returns (bytes32[] memory, address[] memory, uint[] memory) {
        bytes32[] memory name = new bytes32[](5);
        address[] memory owner = new address[](5);
        uint[] memory price = new uint[](5);
        uint j = 0;
        for (uint i = 0; i < 5; i++) {
            if (deals[i].status == Status.Selling && deals[i].end_time > now) {
                Deal storage d = deals[i];
                Property storage prop = properties[d.idOfProperty];
                name[j] = prop.name;
                owner[j] = prop.owner;
                price[j] = d.price;
                j++;
                if (j == 5) {
                    break;
                }
            }
        }
        return (name, owner, price);
    }

    //почему external
    //возвращает все нужные индексы элементов для личного кабинета
    //на фронте фильтруем в нужные окна
    function getArrayOfIndexForAddress(address owner) external view returns (uint[] memory) {
        uint[] memory ok = new uint[](properties.length);
        uint j = 0;
        for (uint i = 0; i < properties.length; i++) {
            if (properties[i].owner == owner) {
                ok[j] = i;
                j++;
            }
        }
        return ok;
    }

    function getArrayOfIndexForWait(address owner) external view returns (uint[] memory) {
        uint[] memory ok = new uint[](deals.length);
        uint j = 0;
        for (uint i = 0; i < deals.length; i++) {
            if (deals[i].new_owner == owner) {
                ok[j] = i;
                j++;
            }
        }
        return ok;
    }

    function newProperty(address _owner, bytes32 _name, uint16 _area, uint16 _u_area) public {
        require(msg.sender == admin, 'Ты не админ!');

        properties.push(
            Property({
                name: _name,
                owner: _owner,
                area: _area,
                u_area: _u_area,
                idOfDeal: 0
              })
        );
    }

    function putOnSale(uint id, uint _price, uint _time) public {
        require(msg.sender == properties[id].owner, 'Ты не собственник!');
        require(properties[id].idOfDeal == 0, 'Объявление уже активно!');

        uint dealId = findClosedDeal();
        if (dealId == 0) {
            deals.push(Deal({
                price: _price,
                end_time: now + _time,
                status: Status.Selling,
                new_owner: address(0),
                new_accept: false,
                idOfProperty: id
            }));
            properties[id].idOfDeal = deals.length - 1;
        } else {
            deals[dealId].price = _price;
            deals[dealId].end_time = now + _time;
            deals[dealId].status = Status.Selling;
            deals[dealId].idOfProperty = id;
            properties[id].idOfDeal = dealId;
        }

    }

    function cancelDeal(uint id) public {
        Deal storage currentDeal = deals[properties[id].idOfDeal];
        require(msg.sender == properties[id].owner, 'Ты не собственник!');
        require(currentDeal.status != Status.Closed, 'Объявление уже неактивно!');

        if (!currentDeal.new_accept) {
            currentDeal.status = Status.Closed;
            properties[id].idOfDeal = 0;
            //event
        } else {
            currentDeal.status = Status.Closed;
            pendingReturns[msg.sender] -= currentDeal.price;
            pendingReturns[currentDeal.new_owner] += currentDeal.price;
            currentDeal.new_owner = address(0);
            properties[id].idOfDeal = 0;
            //event
        }

    }

    function buyProperty(uint id) public payable {
        Deal storage currentDeal = deals[properties[id].idOfDeal];

        require(currentDeal.end_time < now, 'Время сделки истекло!');
        require(currentDeal.status == Status.Selling, 'Не продается!');
        require(currentDeal.price <= msg.value, 'Неправильная сумма!');
        require(currentDeal.new_owner == address(0), 'Извини, но кто-то успел до тебя!');

        currentDeal.new_accept = true;
        currentDeal.status = Status.WaitAccept;
        currentDeal.new_owner = msg.sender;
        pendingReturns[msg.sender] += msg.value - currentDeal.price;
        pendingReturns[properties[id].owner] += currentDeal.price;

        //emit changedStatus(id);
    }

    function checkTime(uint id) public {
        require(deals[properties[id].idOfDeal].end_time > now, 'Время не истекло');

        Deal storage currentDeal = deals[properties[id].idOfDeal];
        currentDeal.status = Status.Closed;
        properties[id].idOfDeal = 0;
        if (currentDeal.new_accept) {
            pendingReturns[msg.sender] += currentDeal.price;
            pendingReturns[properties[id].owner] -= currentDeal.price;
            currentDeal.new_owner = address(0);
        }

          //event

    }

    function oldOwnerWithdrawForSale(uint id) public {
        Deal storage currentDeal = deals[properties[id].idOfDeal];
        require(msg.sender == properties[id].owner, 'Ты не собственник!');
        require(currentDeal.status != Status.WaitAccept, 'Объект еще никто не купил!');

        if (currentDeal.end_time < now) {
            currentDeal.status = Status.Closed;
            pendingReturns[msg.sender] -= currentDeal.price;
            pendingReturns[currentDeal.new_owner] += currentDeal.price;
            currentDeal.new_owner = address(0);
            //event
            return;
        } else {
            pendingReturns[msg.sender] -= currentDeal.price;
            if (!msg.sender.send(currentDeal.price)) {
                pendingReturns[msg.sender] += currentDeal.price;
                //event
            }
        }
        currentDeal.new_owner = address(0);
        properties[id].owner = currentDeal.new_owner;
        currentDeal.status = Status.Closed;
        //event
    }



    function findClosedDeal() private view returns (uint) {
      for (uint i = 0; i < deals.length; i++) {
        if (i != 0 && deals[i].status == Status.Closed) {
          return i;
        }
      }
      return 0;
    }
}
